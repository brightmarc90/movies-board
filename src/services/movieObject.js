export const movieObject = {
    title: "",
    release_date: "",
    categories: [],
    description: "",
    poster: "https://via.placeholder.com/342x513",
    backdrop: "https://via.placeholder.com/600x800",
    actors: [],
    similar_movies: []
    }