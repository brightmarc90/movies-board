const axios = require('axios');

export async function getAll() {
    try {
        const response = await axios.get('http://localhost:3000/movies');
        return(response.data);
    } catch (error) {
        console.error(error);
    }
}

export async function getOne(params) {
    try {
        const response = await axios.get('http://localhost:3000/movies/' + params);
        return (response.data);
    } catch (error) {
        console.error(error);
    }
}

export async function createOne(obj) {
    try {
        const response = await axios.post('http://localhost:3000/movies', obj)
        return (response);
    } catch (error) {
        console.error(error);
    }
}

export async function deleteOne(movieId) {
    try {
        const response = await axios.delete('http://localhost:3000/movies/'+movieId)
        return (response);
    } catch (error) {
        console.error(error);
    }
}