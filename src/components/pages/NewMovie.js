import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import { moviesCateg } from '../../services/categories'
import { movieObject } from '../../services/movieObject';
import { createOne } from '../../services/crud';
import NewMovieInfo from '../NewMovieInfo'

const NewMovie = () => {
    const [movieObj, setMovieObj] = useState(movieObject)
    const [sltCategories, setSltCategories] = useState([])
    const [actorsList, setActorsList] = useState([])
    const [smMovieList, setSmMovieList] = useState([])
    let categCtnr = null, posterInput = "", backDropInput = ""
    const history = useHistory()

    function handleSelect(e) {
        if (!sltCategories.includes(e.target.value)) {
            setSltCategories([...sltCategories, e.target.value])
            const categPill = document.createElement("span")
            categPill.textContent = e.target.value
            categCtnr.appendChild(categPill)
            setMovieObj({...movieObj, categories: sltCategories})
        }        
    }
    
    function setCategCtnr(e) {
        categCtnr = e
    }
    function setPoster(e) {
        posterInput = e
    }
    function setBackDrop(e) {
        backDropInput = e
    }

    function ctrlPosterInput() {
        posterInput.nextSibling.style.display = "block"
        posterInput.focus()
    }
    function ctrlBackDroInput() {
        backDropInput.nextSibling.style.display = "block"
        backDropInput.focus()
    }
    async function execAsynchrone() {
        (async function execAsync() {
            const response = await createOne(movieObj)
            if (response.status === 201) {
                alert("Votre film a été ajouté")
                history.push('/')
            }
        })()
    }

    function handleSubmit(e) {
        e.preventDefault()
        if(posterInput.value){
            if (backDropInput.value) {
                if (!posterInput.value.startsWith("https://") && !posterInput.value.startsWith("http://")) {
                    ctrlPosterInput()
                }
                else {
                    posterInput.nextSibling.style.display = "none"
                    if (!backDropInput.value.startsWith("https://") && !backDropInput.value.startsWith("http://")) {
                        ctrlBackDroInput()
                    } else {
                        execAsynchrone()
                    }
                }
            }else{
                if (!posterInput.value.startsWith("https://") && !posterInput.value.startsWith("http://")) {
                    ctrlPosterInput()
                }else{                    
                    execAsynchrone()
                }
            }
        }else{
            if (backDropInput.value) {
                if (!backDropInput.value.startsWith("https://") && !backDropInput.value.startsWith("http://")) {
                    ctrlBackDroInput()
                } else {                    
                    execAsynchrone()
                }
            } else {                
                console.log(movieObj)
                execAsynchrone()
            }
        }        
    }

    function handleInputChange(e) {
        const { name, value } = e.target;
        setMovieObj({ ...movieObj, [name]: value })
        // console.log(movieObj)
    }
    return (
        <main>
            <h2>Ajouter un film</h2>            
            <section>
                <h3>Rechercher et importer</h3>
                <form action="">
                    <div>
                        <label htmlFor="s_title">Titre</label>
                        <input type="text" name="s_title" id="s_title" />
                    </div>
                    <div>
                        <label htmlFor="s_rel_date">Date de sortie</label>
                        <input type="date" name="s_rel_date" id="s_rel_date" />
                    </div>
                    <div>
                        <label for="s_results">Résultats</label>
                        <input list="s_results_list" id="s_results" name="s_results" />
                        <datalist id="s_results_list">
                            <option value="Spider man"/>
                            <option value="Iron man 1"/>
                            <option value="Iron man 2"/>
                            <option value="Dead pool 1"/>
                            <option value="Dead pool 2"/>
                        </datalist>
                    </div>
                </form>
            </section>
            <section>
                <h3>En saisissant</h3>
                <form action="" onSubmit={(e) => handleSubmit(e)} >
                    <div>
                        <label htmlFor="title">Titre</label>
                        <input type="text" name="title" id="title" required onChange={(e) => handleInputChange(e)} />
                    </div>
                    <div>
                        <label htmlFor="release_date">Date de sortie</label>
                        <input type="date" name="release_date" id="release_date" required onChange={(e) => handleInputChange(e)} />
                    </div>
                    <div>
                        <label htmlFor="categories">Catégories</label>
                        <select id="categories" name="categories" required onChange={(e) => handleSelect(e)} >
                            <option value="99">Sélectionner une valeur</option>
                            {
                                moviesCateg.map(categ => (
                                    <option value={categ}>{categ}</option>
                                ))
                            }
                        </select>
                        <div ref={(e) => setCategCtnr(e)}>                            
                        </div>
                    </div>
                    <div>
                        <label htmlFor="description">Description</label>
                        <textarea name="description" id="description" required onChange={(e) => handleInputChange(e)} />
                    </div>
                    <div>
                        <label htmlFor="poster">Affiche</label >
                        <input type="text" name="poster" id="poster" ref={(e) => setPoster(e)} onChange={(e) => handleInputChange(e)} />
                        <p style={{ display: "none" }}>Ce champ doit impérativement commencer par https:// ou http://</p>
                    </div>
                    <div>
                        <label htmlFor="backdrop">Arrière plan</label>
                        <input type="text" name="backdrop" id="backdrop" ref={(e) => setBackDrop(e)} onChange={(e) => handleInputChange(e)} />
                        <p style={{ display: "none" }}>Ce champ doit impérativement commencer par https:// ou http://</p>
                    </div>
                    <div>
                        <label htmlFor="actors">Acteurs</label>
                        <button name="actors" id="actors">Nouvel acteur</button>
                        <NewMovieInfo actor={true} />
                    </div>
                    <div>
                        <label htmlFor="similar_movies">Films similaires</label>
                        <button name="similar_movies" id="similar_movies">Nouveau film</button>
                        <NewMovieInfo sm_movie={true} />
                    </div>
                    <input type="submit" value="Envoyer" />
                </form>                
            </section>
        </main>
    )
}

export default NewMovie
