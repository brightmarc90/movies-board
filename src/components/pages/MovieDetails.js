import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import MovieCard from '../MovieCard';
import MovieInfoCard from '../MovieInfoCard';
import { getOne } from '../../services/crud';

const MovieDetails = () => {
    const movieId = useParams().id    
    const [movie, setMovie] = useState({})
    let printedDetails

    useEffect(() => {
        (async function execAsync() {
            setMovie(await getOne(movieId))
        }) ()
    }, [])

    if (movie) {
        printedDetails = <>
            <section id="header">
                <div className="container">
                    <h2>{movie.title}</h2>
                </div>                
            </section>
            <section id="infos">
                <div className="container">
                    <div>
                        <MovieCard movie={movie} movieDetail={true} />
                        {
                            movie.actors && (
                                <div>
                                    <ul>
                                        {movie.actors.map(actor => (
                                            <li key={actor.name}> <MovieInfoCard movieInfo={actor} relatedActor={true} /> </li>
                                        ))}
                                    </ul>
                                </div>
                            )
                        }
                    </div>
                    <div>
                        {
                            movie.categories && (
                                <section>
                                    <h3>Catégories</h3>
                                    <p>
                                        {movie.categories.map((category, ind) => (
                                            (movie.categories.length === ind + 1) ? category : category + ", "
                                        ))}
                                    </p>
                                </section>
                            )
                        }
                        {
                            movie.similar_movies && (
                                <section>
                                    <h3>Films similaires</h3>
                                    <ul>
                                        {
                                            movie.similar_movies.map(sim_movie => (
                                                <li> <MovieInfoCard movieInfo={sim_movie} relatedMovie={true} /> </li>
                                            ))
                                        }
                                    </ul>
                                </section>
                            )
                        }
                    </div>
                </div>                
            </section>
        </>
    } else {
        printedDetails = <p>Aucun détail trouvé pour ce film</p>
    }

    return (
        <main>
            {printedDetails}            
        </main>
    );
}

export default MovieDetails;
