import React, {useState, useEffect } from 'react'
import MovieCard from '../MovieCard'
import { getAll } from '../../services/crud'
import { moviesCateg } from '../../services/categories'
import '../../css/AllMovies.css'

const AllMovies = () => {
    const [moviesList, setMoviesList] = useState([])
    let printedMovies

    useEffect(() => {        
        (async function execAsync() {
            setMoviesList(await getAll())
        }) ()              
    }, [])

    function removeDeletedMovie(movieId) {
        setMoviesList(moviesList.filter(elmt => elmt.id !== movieId))
    }

    if (moviesList) {
        printedMovies = (<section id="results" className="py-30">
            <div className="container">
                <h2 className="text-white">Résultats</h2>
                <ul className="d-flex">
                    {moviesList.map(movie => (
                        <li key={movie.id}>{<MovieCard movie={movie} removeDeletedMovie={removeDeletedMovie} allMovie={true} />}</li>
                    ))}
                </ul>
            </div>            
        </section>)
    }else{
        printedMovies = <p>Aucun résultat</p>
    }

    return (
        <main>            
            <section id="filters">
                <div className="container">
                    <h2>Films de la bibliothèque</h2>
                    <p className="mr-20">Filtrer par</p>
                    <form className="d-flex">
                        <div>
                            <label htmlFor="title">Titre</label>
                            <input type="text" name="title" id="title" />
                        </div>
                        <div>
                            <label htmlFor="rel_date">Date de Sortie</label>
                            <input type="date" name="rel_date" id="rel_date" />
                        </div>
                        <div>
                            <label htmlFor="category">Catégories</label>
                            <select name="category" id="category">
                                <option value="0">&nbsp;</option>
                                {
                                    moviesCateg.map(categ => (
                                        <option value={categ}>{categ}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </form>
                </div>                
            </section>
            {printedMovies}
        </main>
    )
}

export default AllMovies