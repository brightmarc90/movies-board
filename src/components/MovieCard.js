import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { deleteOne } from '../services/crud'
import { FaPencilAlt, FaTrashAlt } from 'react-icons/fa';

const MovieCard = ({ movie, removeDeletedMovie, allMovie, movieDetail }) => {
    const history = useHistory()
    let isDeleteClick = false
    let descriptionBloc

    function handleDelete() {
        if(window.confirm("Cliquer sur Ok pour supprimer ce film")){
            (async function execAsync() {
                const response = await deleteOne(movie.id)
                if (response.status === 200) {
                    alert("Votre film a été supprimé")  
                    if (allMovie){
                        removeDeletedMovie(movie.id)
                    }
                    if(movieDetail){
                        history.push("/")
                    }
                }
            })()
        }
        isDeleteClick = true
    }

    function handleLinkClick(e) {
        e.preventDefault()
        if(!isDeleteClick){
            history.push("/movie-details/" + movie.id)
        }
        isDeleteClick = false
    }
    if(allMovie){
        descriptionBloc = <p>{movie.description.length > 110 ? movie.description.substr(0, 100) + "..." : movie.description}</p>
    }else{
        descriptionBloc = <p>{movie.description}</p>
    }
    return (
        <Link to={"/movie-details/"+movie.id} onClick={(e) => handleLinkClick(e)} >
            <figure>
                <div>
                    <img src={movie.poster} alt="Affiche du film" width="230"/>
                    <button className="btn-action btn-update"><FaPencilAlt/></button>
                    <button onClick={handleDelete} className="btn-action btn-delete"><FaTrashAlt/></button>
                </div>
                <figcaption>
                    <h3>{movie.title}</h3>
                    <time dateTime={movie.release_date}>{new Date(movie.release_date).toLocaleDateString("fr-FR")}</time>
                    {descriptionBloc}
                </figcaption>
            </figure>
        </Link>
    )
}

export default MovieCard
