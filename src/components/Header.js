import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import NavBar from './NavBar'
import AllMovies from './pages/AllMovies'
import MovieDetails from './pages/MovieDetails'
import NewMovie from './pages/NewMovie'
import '../css/Header.css'

const Header = () => {
    return(
        <Router>
            <header className="w-100 py-5">
                <div className="container d-grid">
                    <div className="navbar-brand">
                        <h1 className="my-0"><Link to="/"><span>Movies</span> Board</Link></h1>
                    </div>
                    <NavBar/>
                </div>
            </header>
            <Switch>
                <Route path="/" exact>
                    <AllMovies/>
                </Route>
                <Route path="/add-movie">
                    <NewMovie/>
                </Route>
                <Route path="/movie-details/:id">
                    <MovieDetails />
                </Route>
            </Switch>
        </Router>                       
    )
}

export default Header