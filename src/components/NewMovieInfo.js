import React from 'react'

function NewMovieInfo(props) {
    let printedBlock

    if(props.actor){
        printedBlock = (
            <div>
                <div>
                    <label htmlFor="actor_name">Nom complet</label>
                    <input type="text" name="actor_name" id="actor_name" />
                </div>
                <div>
                    <label htmlFor="actor_photo">Photo</label>
                    <input type="text" name="actor_photo" id="actor_photo" />
                </div>
                <div>
                    <label htmlFor="actor_role">Rôle</label>
                    <input type="text" name="actor_role" id="actor_role" />
                </div>
                <button>Ajouter</button>
            </div>
        )
    }else{
        printedBlock = (
            <div>
                <div>
                    <label htmlFor="smv_title">Nom complet</label>
                    <input type="url" name="smv_title" id="smv_title" />
                </div>
                <div>
                    <label htmlFor="actor_photo">Photo</label>
                    <input type="text" name="actor_photo" id="actor_photo" />
                </div>
                <div>
                    <label htmlFor="actor_role">Rôle</label>
                    <input type="text" name="actor_role" id="actor_role" />
                </div>
                <button>Ajouter</button>
            </div>
        )
    }
    return (        
        printedBlock
    )
}

export default NewMovieInfo
