import React from "react";
import '../css/Footer.css'

const Footer = () => {
    return(
        <footer className="py-5">
            <div className="container">
                <p className="text-center">
                    Movies Board © Copyright 2021
                </p>
            </div>            
        </footer>
    )
}

export default Footer