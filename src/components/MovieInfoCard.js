import React from 'react'

function MovieInfoCard(props) {
    const movieInfo = props.movieInfo
    let printedInfo
    if (props.relatedActor){
        printedInfo = (
            <figure>
                <img src={movieInfo.photo} alt={"Photo de " + movieInfo.name} width="70" height="70" />
                <figcaption>
                    <p>{movieInfo.name}</p>
                    <p>{movieInfo.character}</p>
                </figcaption>
            </figure>
        )    
    }else{
        printedInfo = (
            <figure>
                <img src={movieInfo.poster} alt={"Affiche de " + movieInfo.title} width="80" height="80" />
                <figcaption>
                    <p>{movieInfo.title}</p>
                    <p>{new Date(movieInfo.release_date).toLocaleDateString("fr-FR")}</p>
                </figcaption>
            </figure>
        )
    }
    return (
        printedInfo
    )
}

export default MovieInfoCard
