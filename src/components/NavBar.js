import React from 'react'
import { Link } from 'react-router-dom'

const NavBar = () => {
    return(
        <nav>
            <ul className="d-flex unstyled pl-0">
                <li><Link to="/">Tous les films</Link> </li>
                <li><Link to="/add-movie">Nouveau Film</Link></li>
            </ul>
        </nav>
    )
}

export default NavBar
